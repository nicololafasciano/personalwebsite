import Vue from 'vue'

import VuePageTransition from 'vue-page-transition'

Vue.use(VuePageTransition)

const routes = [
  {
    path: '/',
    component: () => import('layouts/Layout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue'), meta: { transition: 'overlay-down-full' } },
      // { path: '', component: () => import('pages/WIP.vue'), meta: { transition: 'overlay-right' } },
      { path: 'home', component: () => import('pages/Home.vue'), meta: { transition: 'overlay-up-full' } }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
